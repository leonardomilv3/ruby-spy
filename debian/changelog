ruby-spy (1.0.5-1) UNRELEASED; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + ruby-spy: Add Multi-Arch: foreign.

  [ Leonardo Milomes Vitoriano ]
  * New upstream version 1.0.5

 -- Leonardo Milomes Vitoriano <leonardomilov@gmail.com>  Wed, 04 Oct 2023 21:16:04 -0300

ruby-spy (1.0.1-1) unstable; urgency=medium

  * Team upload.

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Use secure URI in debian/watch.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Apply multi-arch hints.
    + ruby-spy: Add :any qualifier for ruby dependency.

  [ Lucas Kanashiro ]
  * New upstream version 1.0.1 (Closes: #996512)
  * Update patches
  * Add b-d on ruby-pry-byebug
  * Add b-d on ruby-minitest-reporters
  * Add runtime dependency on ${ruby:Depends}
  * d/watch: bump to version 4 and update to use gemwatch.d.n
  * Bump debhelper compatibility level to 13
  * Declare compliance with Debian Policy 4.6.0
  * d/control: rules does not require root

 -- Lucas Kanashiro <kanashiro@debian.org>  Fri, 05 Nov 2021 16:49:06 -0300

ruby-spy (0.4.3-1) unstable; urgency=medium

  * Bump debhelper compatibility level to 9
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.8 (no changes needed)
  * Run wrap-and-sort on packaging files
  * Imported Upstream version 0.4.3

 -- Cédric Boutillier <boutil@moszumanska.debian.org>  Wed, 06 Jul 2016 22:40:48 +0200

ruby-spy (0.4.2-1) unstable; urgency=medium

  * Initial release

 -- Cédric Boutillier <boutil@debian.org>  Thu, 23 Jul 2015 06:07:00 +0200
